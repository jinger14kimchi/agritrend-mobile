import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable()

export class HttpService {
	constructor(private http: HttpClient){
    }

    getUsers(){
		return this.http.get("http://localhost/agritrend-web/api/users/users");
    }

    signup(params){        
      let header = { "headers": {"Content-Type": "application/json"} };
        return this.http.post( "http://localhost/agritrend-web/api/account/signup", params, header);
    }

    login(params){
        return this.http.post("http://localhost/agritrend-web/api/account/login", params);
    }
    
    getAllCategory() {
        return this.http.get("http://localhost/agritrend-web/api/category/category");
    } 

    getProductByCategory(id) {
        console.log('id cat, htttp', id)
        return this.http.get("http://localhost/agritrend-web/api/product/category?id="+id );
    }

    getLatestReport() {        
        // gets latest date found in monitoring and show all prev in that date
        return this.http.get("http://localhost/agritrend-web/api/prices/reports");
    }

    getProductReport(id) {
        return this.http.get("http://localhost/agritrend-web/api/trends/latest?id="+id);
    }
    getMarket() {
        return this.http.get("http://localhost/agritrend-web/api/market/market");
        
    }
    getProduct() {
        return this.http.get("http://localhost/agritrend-web/api/product/product");
    }

    getProductUnits() {
        return this.http.get("http://localhost/agritrend-web/api/product/units");
    }

    getData() {
        return this.http.get("http://localhost/agritrend-web/api/product/units");
    }
    
   
    update(params) {        
        return this.http.post("http://localhost/agritrend-web/api/account/update", params);
    }

    postPriceForm(params){
        return this.http.post("http://localhost/agritrend-web/api/prices/prices", params);
    }

    getProfile(params){
        let id = params.id
		return this.http.get("http://localhost/agritrend-web/api/users/users?id" + id);
    }
}