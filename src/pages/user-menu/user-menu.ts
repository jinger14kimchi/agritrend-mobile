import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { MonitoringPage } from '../monitoring/monitoring';
import { ReportsPage } from '../reports/reports';
import { AnalysisPage } from '../analysis/analysis';
import { AccountManagementPage } from '../account-management/account-management';
import { ProductManagementPage } from '../product-management/product-management';
import { Storage } from '@ionic/storage';
import { HomePage } from '../home/home';
import { ProfilePage } from '../profile/profile';


@IonicPage()
@Component({
  selector: 'page-user-menu',
  templateUrl: 'user-menu.html',
})
export class UserMenuPage {

	ReportsPage: ReportsPage;
	AnalysisPage: AnalysisPage;
  ProductManagementPage: ProductManagementPage;
  HomePage: HomePage;
  ProfilePage: ProfilePage;

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    public storage: Storage) {
  }
  logout() {
    this.storage.remove('username');
    console.log('remove local storage');
    this.navCtrl.setRoot(HomePage);
  }
  profile(){
    this.navCtrl.push(ProfilePage);
  }

  gotoAccountMngt() {
    this.navCtrl.push(AccountManagementPage);
  }

  gotoProductMngt() {
    this.navCtrl.push(ProductManagementPage);
  }

  gotoPriceReports() {
  	this.navCtrl.push(ReportsPage);
  }

  gotoPriceMonitoring() {
  	this.navCtrl.push(MonitoringPage);

  }

  gotoAnalysis() {
  	this.navCtrl.push(AnalysisPage);
  	
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UserMenuPage');
  }

}
