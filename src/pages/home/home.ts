import { Component } from '@angular/core';
import { NavController} from 'ionic-angular';
import { Storage } from '@ionic/storage';

import 'rxjs/add/operator/map';

import { LoginPage } from '../login/login';
import { SignupPage } from '../signup/signup';
import { MainMenuPage } from '../main-menu/main-menu'; 
import { UserMenuPage } from '../user-menu/user-menu'; 

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
	LoginPage: LoginPage;
	SignupPage: SignupPage;
	UserMenuPage: UserMenuPage;
	user: any;
  constructor(public navCtrl: NavController, public storage: Storage) {
		this.checkStorage();
  }
 
	checkStorage() {
		this.storage.get('username').then((val) => {			
			if (val != null) {
				console.log('username: ', val);
				this.storage.get('user_type').then((val) => {
					if (val == 'admin' || val == 'agent') {
						console.log('user_type: ', val)
						this.gotoMainPage();
					}
					else {
						this.gotoUserMenuPage();
					}
				});
			}
		});
	}

	gotoUserMenuPage() {
		this.navCtrl.setRoot(UserMenuPage);
	}
	gotoLoginPage() {
	  this.navCtrl.push(LoginPage);
	}
	
	gotoMainPage() {		
	  	this.navCtrl.setRoot(MainMenuPage);
	}
  gotoSignupPage() {
	  this.navCtrl.push(SignupPage);
	}
}
