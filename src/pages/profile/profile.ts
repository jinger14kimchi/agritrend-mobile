import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ModalController } from 'ionic-angular';
import{ Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import {HttpService} from '../../services/http.service'; 

@Injectable()
@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
  dataArray: any;
  user: any;
  user_id: any;
  fname: any;
  lname: any;
  gender: any;
  username: any;
  birthdate: any;
  

  constructor(public navCtrl: NavController,
     public navParams: NavParams,
     public http: HttpService,
     private storage: Storage,
     public alertCtrl: AlertController,
     public modalController: ModalController) {
       this.doGetProfile()
      this.storage.get('username').then((username) => {

        this.user  = username;
        console.log(username);
       
      });
     
      this.storage.get('user_id').then((user_id) => {

        this.user_id  = user_id;
        console.log('user_id ', user_id);
       
      });

  }

 
  doGetProfile(){ 
    let params ={
      'user_id': this.user_id
    }
   
    this.http.getProfile(params).subscribe(
      (response) =>{
        let data = response[0];
        this.birthdate = data.birthdate;
        this.fname = data.fname;
        this.lname = data.lname;
        this.username = data.username;
        this.user = data.fname + ' ' + data.lname;
        this.gender = data.gender;
        this.dataArray= data;
        console.log(data);
    },
    (error) => { 
      console.log(error);
    });
  }

  
  updateProfile() {
    let params ={
      'user_id': this.user_id,
      'username': this.username,
      'gender': this.gender,
      'birthdate': this.birthdate,
      'fname': this.fname,
      'lname': this.lname
    }
    if(params != null){
      console.log('params', params)
      this.http.update(params).subscribe(
        (response) =>{
          console.log('response', response);
          this.showAlert("Successfully Updated!");
        },
        (error) => { 
          console.log(error);
          this.showAlert("Update Failed!");
        });
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }
  
  showAlert(msg){
    const alert = this.alertCtrl.create({
      
      subTitle: msg,
      buttons:['OK']

    });
    alert.present();
}

}
