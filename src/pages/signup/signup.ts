import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { HttpService } from '../../services/http.service';
import { LoginPage } from '../login/login';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'signup.html',
})
export class SignupPage {
  username;
  password;
  birthdate;
  gender;
  fname;
  lname;
  user_type = 'normal';
  LoginPage: LoginPage;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public http: HttpService, 
    public alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
  }

  signup() {
    let params = {
      'username': this.username,
      'password': this.password,
      'fname': this.fname,
      'lname': this.lname,
      'gender': this.gender,
      'birthdate': this.birthdate,
      'user_type': this.user_type
    }

    this.http.signup(params).subscribe(
      (response) => {
        console.log('params ', params )
        console.log('response', response['message']);
        this.showAlert(response['message']);
        this.navCtrl.setRoot(LoginPage);

      },
      (error) => {
        console.log('error', error)
      }
    );
  }


  showAlert(msg){
    const alert = this.alertCtrl.create({      
      subTitle: msg,
      buttons:['OK']

    });
    alert.present();
}
}
 