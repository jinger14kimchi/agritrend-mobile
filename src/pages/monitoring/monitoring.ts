import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController} from 'ionic-angular';
import { Storage } from '@ionic/storage';
import {HttpService} from '../../services/http.service'; 
import 'rxjs/add/operator/map';
import{ Injectable } from '@angular/core';

@Injectable()
@IonicPage()
@Component({
  selector: 'page-monitoring',
  templateUrl: 'monitoring.html',
})
export class MonitoringPage {
  dataArraymarket: any;
  dataArrayproduct: any; 

  market_id;
  store;
  product_id;
  unit;
  source;
  price;
  user_id;
  constructor(public navCtrl: NavController,
     public navParams: NavParams,
     public http: HttpService,
     public alertCtrl: AlertController,
     private storage: Storage
     ) {
       this.getMarket();
       this.getProduct(); 
       this.storage.get('user_id').then((user_id) => {

        this.user_id  = user_id;
        console.log('user_id ', user_id);
       
      });
  }
 getMarket(){   
    this.http.getMarket().subscribe(
      (response) =>{
      this.dataArraymarket= response;
      console.log(this.dataArraymarket);
  },
      (error) => { console.log(error);
      }
    )
  }
getProduct(){
  this.http.getProduct().subscribe(
    (response) =>{
    this.dataArrayproduct= response;
    console.log(this.dataArrayproduct);
},
    (error) => { console.log(error);
    }
  )
}

saveMonitor(){
  let params ={
    "market_id": this.market_id,
    "store": this.store ? this.store : "NA",
    "product_id": this.product_id,
    "unit": this.unit,
    "price": this.price,
    "source": this.source ? this.source : "",
    "user_id": this.user_id    
  }

  this.http.postPriceForm(params).subscribe(
    (response) => {
      console.log('params ', params )
      
      console.log('response', response);

      this.showAlert("Successfully Added");
      
    },
    (error) => {
      console.log('error', error)
      console.log('params ', params )
      this.showAlert("input valid");
    }
  ); 

}
  ionViewDidLoad() {
    console.log('ionViewDidLoad MonitoringPage');
  }
  showAlert(msg){
        const alert = this.alertCtrl.create({          
          subTitle: msg,
          buttons:['OK']

        });
        alert.present();
    }
}
