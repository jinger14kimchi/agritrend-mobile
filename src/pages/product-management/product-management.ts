import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { HttpService } from '../../services/http.service'; 
import { LoadingController } from 'ionic-angular';
import 'rxjs/add/operator/map';
import{ Injectable } from '@angular/core';
import { ProductPage } from '../product/product';

@Injectable()
@IonicPage()
@Component({
  selector: 'page-product-management',
  templateUrl: 'product-management.html',
})
export class ProductManagementPage {

  search :any;
  dataArrayproduct: any;
  filterCategory: any;
  categoryList: any;
  category: any;
  product: any;
  ProductPage: ProductPage;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public http: HttpService, 
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController  ) {
      this.getAllCategories();
  } 
 
  getAllCategories() {
    this.http.getAllCategory().subscribe(
      (response) => {
        let data = response;
        this.categoryList = data;
        console.log('categories ', this.categoryList);
      },
      (error) => {
        console.log(error)
      }
    );
  }

  changeCategory() {
    console.log('Changing category');
    let cat = this.category;
    console.log('trying to get cat', cat);
    this.http.getProductByCategory(cat).subscribe(
      (response) => {
        this.dataArrayproduct = response;
        console.log('response', response);
      },
      (error) => {
        console.log(error);
      }
    );
  }

  showProductReport(event) {
    let prod_id = event.target.id;
    console.log(event);
    this.http.getProductReport(prod_id).subscribe(
      (response) => {
        console.log('response', response);
        this.navCtrl.push(ProductPage, {
          data: response[0]
        });
      },
      (error) => {
        console.log('error', error);
        this.showAlert("Sorry, there's no available reports for <strong>"+event.target.innerText+"</strong>");

      }
      );
  }

  getProduct(){
    this.http.getProduct().subscribe(
      (response) =>{
        this.dataArrayproduct= response;
        console.log('products', this.dataArrayproduct);
    },
      (error) => { console.log(error);
      }
    )
  }

  pageLoading() {
    const loader = this.loadingCtrl.create({
      spinner: 'bubbles',
      duration: 2000
    });
    loader.present();
  }

  showAlert(msg){
    const alert = this.alertCtrl.create({
      
      subTitle: msg,
      buttons:['OK']

    });
    alert.present();
}


  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductManagementPage');
  }

}
