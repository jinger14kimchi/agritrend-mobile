import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController} from 'ionic-angular';
import { HttpService } from '../../services/http.service';
import { Storage } from '@ionic/storage';
import { LoadingController } from 'ionic-angular';

import { MainMenuPage } from '../main-menu/main-menu'; 
import { SignupPage } from '../signup/signup';
import { ProfilePage } from '../profile/profile';
import { UserMenuPage } from '../user-menu/user-menu';


@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
	MainMenuPage: MainMenuPage;
  SignupPage: SignupPage;
  ProfilePage: ProfilePage;
  UserMenuPage: UserMenuPage;

  username;
  password;
  user_id;
  user: any;
  user_type;

  constructor (  
    public navCtrl: NavController,   
    public navParams: NavParams,   
    public http: HttpService,   
    private storage: Storage,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController  ) { 
  }

  login() {
      console.log("logging in");
      let params = {
        'username': this.username,
        'password': this.password
      }
      this.http.login(params).subscribe(
          (response) => {
              console.log('login response', response);
              if (response) {
                let u = response;
                  this.setUserData(u['user'].username, u['user'].user_id, u['user'].user_type) 
                  if (u['user'].user_type == 'agent' || u['user'].user_type == 'admin'){
                    this.showAlert("Welcome, <br>"+ u['user'].fname + ' ' + u['user'].lname);
                    console.log('User is agent or admin')
                    this.navCtrl.setRoot(MainMenuPage); 
                  
                  } 
                  else{
                      console.log('user normal:', this.user_type);
                      this.showAlert("Welcome, <br>"+ u['user'].fname + ' ' + u['user'].lname);
                      this.navCtrl.setRoot(UserMenuPage);
                } 
              }         
            },
            (error) => {
              console.log('error', error)
              this.showAlert("Non existing Account");
            }
        );
  }

  setUserData(username, user_id, user_type) {
    this.storage.set('username', username);
    this.storage.set('user_id', user_id);
    this.storage.set('user_type', user_type);
    console.log('username=', username);
    console.log('user type:', user_type);
    this.gotoMainMenu();
  }

  showAlert(msg){
    const alert = this.alertCtrl.create({
      
      subTitle: msg,
      buttons:['OK']

    });
    alert.present();
}

  gotoMainMenu() {
  	this.navCtrl.setRoot(MainMenuPage);
  }

  gotoSignupPage() {
    this.navCtrl.push(SignupPage);
  }
  
  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

}
