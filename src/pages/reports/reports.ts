import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

import { HttpService } from '../../services/http.service'; 
import { LoadingController } from 'ionic-angular';
import 'rxjs/add/operator/map';
import{ Injectable } from '@angular/core';

@IonicPage()
@Component({
  selector: 'page-reports',
  templateUrl: 'reports.html',
})
export class ReportsPage {

  categoryList: any;
  category: any;
  dataArrayproduct: any;
  start_date: any;
  end_date: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public http: HttpService, 
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController  ){
      this.getAllCategories();
      this.showLatestReport()
  } 
 
  getAllCategories() {
    this.http.getAllCategory().subscribe(
      (response) => {
        let data = response;
        this.categoryList = data;
        console.log('categories ', this.categoryList);
      },
      (error) => {
        console.log(error)
      }
    );
  }

  showLatestReport() {
    console.log('Latest');
    this.http.getLatestReport().subscribe(
      (response) => {
        console.log('reports', response);
        this.dataArrayproduct = response;
      },
      (error) => {
        console.log(error);
      }
    );
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad ReportsPage');
  }

}
