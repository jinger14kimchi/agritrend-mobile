import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-product',
  templateUrl: 'product.html',
})
export class ProductPage {
	productName: any;
	cogonPrice: any;
	carmenPrice: any;
	productInfo: any;
	monitoringDate: any;
	categoryName: any;
	unit: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController) {
  	this.showProductInfo();
  }

  showProductInfo() {
  	let data = this.navParams.get('data');
  	this.productName = data.product_name;
  	this.cogonPrice = data.cogon_prev_price;
  	this.carmenPrice = data.carmen_prev_price;
  	this.categoryName = data.category_name;
  	var d1 = new Date(data.monitoring_date);
	// d1.toString();       //Returns "2009-06-29" in Internet Explorer, but not Firefox or Chrome
	d1.toString('dddd, MMMM ,yyyy') ;
  	this.monitoringDate = d1;
	  	console.log('data from prd mngt', data);
  }

  showAlert(msg){
    const alert = this.alertCtrl.create({
      
      subTitle: msg,
      buttons:['OK']

    });
    alert.present();
}

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductPage');
  }

}
