import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {HttpService} from '../../services/http.service'; 
import 'rxjs/add/operator/map';

@IonicPage() 
@Component({
  selector: 'page-account-management',
  templateUrl: 'account-management.html',
})
export class AccountManagementPage {

  dataArray: any;
  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    private http: HttpService)  {
      this.doGet(); 
  }
 
  
  doGet(){
    this.http.getData().subscribe(
      (response) =>{
      this.dataArray= response;
  },
      (error) => { console.log(error);
      }
    )
    }
  
}
