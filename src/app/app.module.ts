import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { HttpClientModule } from '@angular/common/http';
import { HttpService } from '../services/http.service';

import { IonicStorageModule } from '@ionic/storage';

import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/login/login';
import { SignupPage } from '../pages/signup/signup';
import { MonitoringPage } from '../pages/monitoring/monitoring';
import { ReportsPage } from '../pages/reports/reports';
import { AnalysisPage } from '../pages/analysis/analysis';
import { MainMenuPage } from '../pages/main-menu/main-menu';
import { AccountManagementPage } from '../pages/account-management/account-management';
import { ProductManagementPage } from '../pages/product-management/product-management';
import { ProfilePage } from '../pages/profile/profile';
import { ProductPage } from '../pages/product/product';
import { TablePage } from '../pages/table/table';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { UserMenuPage } from '../pages/user-menu/user-menu';


import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    LoginPage,
    SignupPage,
    MonitoringPage,
    ReportsPage,
    AnalysisPage,
    MainMenuPage,
    AccountManagementPage,
    ProductManagementPage,
    ProfilePage,
    ProductPage,
    TablePage,
    UserMenuPage,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    HttpClientModule,
    NgxDatatableModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    LoginPage,
    SignupPage,
    MonitoringPage,
    ReportsPage,
    AnalysisPage,
    MainMenuPage,
    AccountManagementPage,
    ProductManagementPage,
    ProfilePage,
    ProductPage,
    TablePage,
    UserMenuPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    HttpService
  ]
})
export class AppModule {}
